<?php
/**
 * @file AutomatedDatabase.inc
 */

/**
 * AutomatedDatabase class is meant to provide short automagic notation
 * to avoid db_set_active and db_query madness.
 * 
 * Database description - consider it a Drupal's connection wrapper
 *
 * @author Mołot <Molot@1261030.no-reply.drupal.org>
 */
class AutomatedDatabase {

  /**
   * Array of instances, in form $instances[$target][$key]
   * @var array
   */
  private static $databases = array();

  /**
   * Array of table instances, in form of $tables[$table_name]
   * @var array
   */
  private $tables = array();

  /**
   * Drupal's raw database connection we wrap around
   * @var DatabaseConnection 
   */
  private $connection;
  
  /**
   * Name of the database we are connected to
   * @var string
   */
  private $name;
  
  /**
   * Private constructor to make it just an obiect factory
   */
  private function __construct($key = 'default') {
    $this->connection = Database::getConnection(null, $key);
    $this->name = $key;
    self::$databases[$key] = $this;
  }

  /**
   * When calling object as function, like ShopDatabase(), return connection
   * This allows us to save all actual calls for useful stuff.
   * Any arguments are happily ignored.
   * 
   * @return DatabaseConnection
   */
  public function __invoke() {
    return $this->connection;
  }
  
  /**
   * 
   * @param string $key Database to use
   */
  public static function getDatabase($key) {
    // If not set, create
    if (!isset(self::$databases[$key][$key])) {
      //Was this class properly overriden?
      $preffered_class = 'AutomatedDatabase' . $key;
      $real_key = self::fromCamelCase($key);
      if (class_exists($preffered_class) and in_array('AutomatedDatabase', class_parents($preffered_class))) {
        self::$databases[$real_key] = new $preffered_class($real_key);
      }
      else {
        if (class_exists($preffered_class)) { //if exists and we are not going to use it, warn
          watchdog('Database Automaton',
              'Class %class exists, but is not a child of AutomatedDatabase class and cannot be used.',
              $variables = array('%class' => 'Database Automaton'),
              $severity = WATCHDOG_WARNING);
        }
        self::$databases[$real_key] = new AutomatedDatabase($real_key);
      }
    }
    // return, old or new
    return self::$databases[$real_key];
  }
  
  /**
   * Obtain an instance of table
   * @param string $table_name may be provided in CamelCase, lowerCamel
   *     or under_score conventions, will be converted to under_score
   */
  public function getTable($table_name) {
    $table_real_name = self::fromCamelCase($table_name);
    // If not set, create
    if (!isset($this->tables[$table_name])) { //return table if already exists
      //tricky part - create instance of right class
      //Was this class properly overriden?
      $preffered_class = 'AutomatedTable' . $this->name . $table_name;
      if (class_exists($preffered_class) and in_array('AutomatedTable', class_parents($preffered_class))) {
        //prefer instance() over constructor
        if (method_exists($preffered_class, 'instance')) {
          $this->tables[$table_name] = $preffered_class::instance($this, $table_real_name);
        }
        else { // fall back to creation
          $this->tables[$table_name] = new $preffered_class($this, $table_real_name);
        }
      }
      else {
        if (class_exists($preffered_class)) { //if exists and we are not going to use it, warn
          watchdog('Database Automaton',
              'Class %class exists, but is not a child of AutomatedTable class and cannot be used.',
              $variables = array('%class' => 'Database Automaton'),
              $severity = WATCHDOG_WARNING);
        }
        $this->tables[$table_name] = new AutomatedTable($this, $table_real_name);
      }
    }
    return $this->tables[$table_name];
  }

  /**
   * Function to implement:
   * - getDatabaseName()
   * 
   * Will not be implemented yet:
   * - getDatabaseNameKey()
   * 
   * @param string $name
   * @param array $arguments
   */
  public static function __callStatic($name, $arguments) {
    $name_parts = preg_split('/(?=[A-Z])/', $name);
    //under 0 and 1 should be 'get' and 'Database'
    switch ($name_parts[0]) {
      case 'get':
        switch ($name_parts[1]) {
          case 'Database':
            //under 2 and 3 should be key and target.
            return self::getDatabase(implode('', array_slice($name_parts, 2)));
            break;
          //Place other gets here
          //Default is to get database
          default:
            return self::getDatabase(implode('', array_slice($name_parts, 1)));
            break;
        }
        break;
    }
  }

  /**
   * Automagic function to implement
   * - getTableSomething()
   * - describeTableSomething()
   * @param string $name
   * @param array $arguments
   */
  public function __call($name, $arguments) {
    $name_parts = preg_split('/(?=[A-Z])/', $name);
    //under 0 and 1 should be 'get' and 'Table'
    switch ($name_parts[0]) {
      case 'get':
        switch ($name_parts[1]) {
          case 'Table':
            //under 2+ should be table name.
            return $this->getTable(implode('', array_slice($name_parts, 2)));
            break;
          // other getters here
          // by default, get table
          default:
            return $this->getTable(implode('', array_slice($name_parts, 1)));
        }
        break;
      // other actions here
      // by default, get table
      default :
        return $this->getTable(implode('',$name_parts));
    }
  }

  /**
   * This method converts PHP and Drupal standard
   * ObjectNaming and getMethodNaming convention into table_name one
   * @param string $input
   * @return string
   * @author cletus
   * @link http://stackoverflow.com/questions/1993721/how-to-convert-camelcase-to-camel-case#1993772 StackOverflow answer that provided this code
   */
  public static function fromCamelCase($input) {
    $matches = array();
    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
    $ret = $matches[0];
    foreach ($ret as &$match) {
      $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
    }
    return implode('_', $ret);
  }
  
  /**
   * Function for symmetry with fromCamelCase
   * It's supposed to return strings that would be valid property names under
   * Drupal's OOP naming policy
   * 
   * @link https://drupal.org/node/608152#naming Object-oriented code naming conventions
   * @param string $input String to work on
   * @param boolean $first_lowercase Make first character lowercase, as in property name, or keep it uppercase as in class name?
   * @return string
   */
  public static function toCamelCase($input, $first_lowercase = TRUE) {
    $parts = explode('_', $input);
    $result = '';
    foreach ($parts as $part) {
      $result .= ucfirst(strtolower($part));
    }
    if($first_lowercase) {
      $result = lcfirst($result);
    }
    return $result;
  }
}

//Shorthand alias
class_alias('AutomatedDatabase', 'audb');
