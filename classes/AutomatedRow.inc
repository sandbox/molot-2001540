<?php
/**
 * @file AutomatedRow.inc
 */

/**
 * AutomatedRow class is meant to provide short automagic notation
 * to avoid db_set_active and db_query madness.
 * 
 * Row's description
 *
 * @author Mołot <Molot@1261030.no-reply.drupal.org>
 */
class AutomatedRow implements Iterator {
    
  /**
   * We will delegate actual work do DatabaseStatementInterface
   * This is only a wrapper for easier operations
   * 
   * @var DatabaseStatementInterface
   */
  protected $databaseStatement;
  
  public function __construct() {
    
  }
  
  public function current() {
    $this->databaseStatement->current();
  }

  public function key() {
    $this->databaseStatement->key();
  }

  public function next() {
    $this->databaseStatement->next();
  }

  public function rewind() {
    $this->databaseStatement->rewind();
  }

  public function valid() {
    $this->databaseStatement->valid();
  }
}

