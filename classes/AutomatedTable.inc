<?php
/**
 * @file AutomatedTable.inc
 */

/**
 * AutomatedTable class is meant to provide short automagic notation
 * to avoid db_set_active and db_query madness.
 *
 * Table's description
 *
 * @author Mołot <Molot@1261030.no-reply.drupal.org>
 */
class AutomatedTable {
  /**
   * AutomatedDatabase parent of this table
   * @var AutomatedDatabase
   */
  protected $database;

  /**
   * Name of the table we are connected to
   * @var string
   */
  protected $name;
  
  /**
   * RDBMS engine can be obtained from $this->database at any moment,
   * but it'll be clearer to have it stored ready to use.
   * 
   * @var string engine name
   */
  protected $engine;
  
  /**
   *
   * @var array Names of columns that are part of primary keys
   */
  protected $primary_keys = array();
  
  /**
   * This constructor is public as we see no reason to prevent manual construction.
   * AutomatedDatabase provides factory searching for best match.
   * Child classes are free to make constructor private and implement instance()
   *
   * @param DatabaseConnection $connection
   * @param string $name
   */
  public function __construct(AutomatedDatabase &$database, $name) {
    $this->database = $database;
    $this->name = $name;
    
    if ($database() instanceof DatabaseConnection_pgsql) {
      $this->engine = 'pgsql';
      /**
       * @see http://wiki.postgresql.org/wiki/Retrieve_primary_key_columns
       * @todo Needs testing
       */
      $indexes = $database()->query("
        SELECT               
          pg_attribute.attname AS name
        FROM pg_index, pg_class, pg_attribute 
        WHERE 
          pg_class.oid = 'TABLENAME'::regclass AND
          indrelid = pg_class.oid AND
          pg_attribute.attrelid = pg_class.oid AND 
          pg_attribute.attnum = any(pg_index.indkey)
          AND indisprimary
        ");
      foreach ($indexes as $index) {
        $this->primary_keys[] = $index->name;
      }
    }
    elseif ($database() instanceof DatabaseConnection_mysql) {
      $this->engine = 'mysql';
//      $indexes = $database()->query('SHOW INDEX FROM :table WHERE Key_name = "PRIMARY"', array(':table' => $this->name));
//      foreach ($indexes as $index) {
//        $this->primary_keys[] = $index->Column_name;
//      }
    }
  }

  /**
   * Get rows, either entirety or only given columns
   *
   * @param array $fields array of names (keys ignored)
   * @param array $conditions associative array in one of two forms:
   *   // simple
   *   $conditions = array(
   *     'category' => 5,
   *     'user' => 3,
   *   )
   *
   *   // complex
   *   $conditions = array(
   *     'category' => array(
   *       'value' => array(5, 8),
   *       'operator' => 'IN',
   *       'next' => 'OR',
   *     ),
   *     'user' => 3,
   *   )
   *
   */
  public function get(array $fields, array $conditions) {
    /**
     * @var DatabaseConnection Drupal's database connection object to do the real job.
     */
    $database = $this->database; // Needed until https://bugs.php.net/bug.php?id=64939 gets approved.
    /**
     * @var DatabaseQuery Drupal's query to work with.
     */
    $query = $database()->select($this->name);
    /**
     * @internal Ths if is not really needed now, but I prefer to have structure ready for future needs.
     */
    if (isset($fields) and !empty($fields)) {
      foreach($fields as $field)
        $query->addField($this->name, $field, AutomatedDatabase::toCamelCase($field));
    }
    else {
      $query->fields($this->name);
    }
    
    /**
     * @internal Conditions will take most of the fun as we allow two ways of defining them. It forces us to use if in foreach.
     */
    if (isset($conditions) and !empty($conditions)) {
      foreach ($conditions as $field => $condition) {
        if (is_array($condition)) { // If it is an array, it have to be properly formatted
          /**
           * @todo Implement 'next' functionality.
           * @see http://api.drupal.org/api/drupal/includes!database!database.inc/function/db_or/7
           * @ticket 2006730
           */
          $query->condition($field, $condition['value'], $condition['operator']);
        }
        else { // If not an array we just hope it'll cast to string properly
          $query->condition($field, $condition, '=');
        }
      } // foreach ($conditions as $field => $condition)
    } // if (isset($conditions))
    
    /**
     * @todo return AutomatedRow instead.
     */
    return $query->execute()->fetchObject();
  }

  /**
   * Support for getFieldAndFieldByConditionAndCondition() notation
   * @param string $name
   * @param array $arguments
   */
  public function __call($name, $arguments) {
    $name_parts = preg_split('/(?=[A-Z])/', $name);
    switch ($name_parts[0]) {
    //get is default
    case 'get':
      $parsed = self::parseQueryParts(array_slice($name_parts, 1), $arguments);
      return $this->get($parsed['fields'], $parsed['conditions']);
      break;
    
    //default is just like get, but one wrd earlier
    default:
      $parsed = self::parseQueryParts($name_parts, $arguments);
      return $this->get($parsed['fields'], $parsed['conditions']);
      break;
    }
  }
  
  /**
   * Parses flat array of name parts to create fields and conditions understandable for get() method.
   * 
   * @param array $parts Parts of magic function name to turn into fields and conditions
   * @param array $arguments Values for filters
   * @return array Array with 2 elements, fields and conditions, to call get() with
   */
  private static function parseQueryParts(array $parts, array $arguments = array()) {
    $parsed = array(
      'fields' => array(),
      'conditions' => array(),
    );
    $state = 'fields';
    $name = '';
    $argument_number = 0;
    foreach ($parts as $part) {

      //we finished parsing field or condition name for now
      if (in_array($part, array('By', 'And', 'Or', 'Xor'))) {
        switch ($state) {
          case 'fields':
            $parsed['fields'][] = audb::fromCamelCase($name);
            break;
          
          case 'conditions':
            /**
             * @todo make complex synatx for or and xor happen
             */
            $parsed['conditions'][audb::fromCamelCase($name)] = $arguments[$argument_number];
            $argument_number++;
            break;

          default:
            break;
        }
        $name = '';
      }
      else {
        $name .= $part;
      }
      //We are no longer parsing fields
      if ($part == 'By') {
        $state = 'conditions';
      }
    }
    //At the end, add last element too
    /**
     * @todo De-duplicate code with above section
     */
    switch ($state) {
      case 'fields':
        $parsed['fields'][] = audb::fromCamelCase($name);
        break;

      case 'conditions':
        /**
         * @todo make complex synatx for or and xor happen
         */
        $parsed['conditions'][audb::fromCamelCase($name)] = $arguments[$argument_number];
        $argument_number++;
        break;

      default:
        break;
    }
    return $parsed;
  }
}

